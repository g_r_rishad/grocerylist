package com.example.rishad.mygrocerylist.Activities.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.rishad.mygrocerylist.Activities.Data.DatabaseHandler;
import com.example.rishad.mygrocerylist.Activities.Model.Grocery;
import com.example.rishad.mygrocerylist.R;

public class  MainActivity extends AppCompatActivity {

    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private EditText groceryItem,quantity;
    private Button saveButton;
    private DatabaseHandler db;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //byPassActivity();
        db = new DatabaseHandler(this);
        byPassActivity();
        fab= (FloatingActionButton) findViewById(R.id.fabId);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Snackbar.make(v, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        //.setAction("Action", null).show();
                createPopupDialog();
            }
        });
    }

    public void createPopupDialog(){
        dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.popup,null);
        groceryItem = (EditText) view.findViewById(R.id.groceryItem);
        quantity =(EditText) view.findViewById(R.id.groceryQty);
        saveButton = (Button) view.findViewById(R.id.saveButton);

        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGroceryToDB(v);
            }
        });
    }
    private void saveGroceryToDB(View v) {

        Grocery grocery = new Grocery();
        String newGrocery = groceryItem.getText().toString();
        String newGroceryQuantity = quantity.getText().toString();
        grocery.setName(newGrocery);
        grocery.setQuantity(newGroceryQuantity);
        // save to DB
        db.addGrocery(grocery);
        Snackbar.make(v,"Item Saved !",Snackbar.LENGTH_SHORT).show();
        Log.d("Item Added id :", String.valueOf(db.getGroceriesCount()));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                startActivity(new Intent(MainActivity.this,ListActivity.class));
            }
        },1000);
    }

    public void byPassActivity(){
        // checks if database is empty: if not, then we just go to ListActivity and show all added items
        if(db.getGroceriesCount()>0){
            finish();
            startActivity(new Intent(MainActivity.this,ListActivity.class));
        }
    }
}
