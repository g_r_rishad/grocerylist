package com.example.rishad.mygrocerylist.Activities.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.rishad.mygrocerylist.Activities.Data.DatabaseHandler;
import com.example.rishad.mygrocerylist.Activities.Model.Grocery;
import com.example.rishad.mygrocerylist.Activities.UI.RecyclerViewAdapter;
import com.example.rishad.mygrocerylist.R;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<Grocery> groceryList;
    private List<Grocery> listItems;
    private DatabaseHandler db;
    private Context context;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                   //  .setAction("Action", null).show();

                createPopupDialog();
//                MainActivity ac= new MainActivity();
//                ac.createPopupDialog();
            }
        });

        db=new DatabaseHandler(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        groceryList = new ArrayList<>();
        listItems = new ArrayList<>();
        //get items from database
        groceryList = db.getAllGroceries();

        for(Grocery c : groceryList){
            Grocery grocery = new Grocery();
            grocery.setName(c.getName());
            grocery.setQuantity("Qty: "+c.getQuantity());
            grocery.setId(c.getId());
            grocery.setDateItemAdded("Added on: "+c.getDateItemAdded());

            listItems.add(grocery);
        }
        recyclerViewAdapter = new RecyclerViewAdapter(this,listItems);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.notifyDataSetChanged();
    }

    private void createPopupDialog(){
        dialogBuilder = new AlertDialog.Builder(ListActivity.this);
        View view = getLayoutInflater().inflate(R.layout.popup,null);
        EditText groceryItem = (EditText) view.findViewById(R.id.groceryItem);
        EditText quantity =(EditText) view.findViewById(R.id.groceryQty);
        Button saveButton = (Button) view.findViewById(R.id.saveButton);

        final String newGrocery = groceryItem.getText().toString();
        final String newGroceryQuantity = quantity.getText().toString();

        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGroceryToDB(v,newGrocery,newGroceryQuantity);
            }
        });
    }
    private void saveGroceryToDB(View v,String newGrocery,String newGroceryQuantity) {

        Grocery grocery = new Grocery();

        grocery.setName(newGrocery);
        grocery.setQuantity(newGroceryQuantity);
        // save to DB
        db.addGrocery(grocery);
        Snackbar.make(v,"Item Saved !",Snackbar.LENGTH_SHORT).show();
        Log.d("Item Added id :", String.valueOf(db.getGroceriesCount()));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                startActivity(new Intent(ListActivity.this,ListActivity.class));
            }
        },1000);
    }

}
